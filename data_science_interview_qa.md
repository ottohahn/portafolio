# Data Science Interview Questions

From: A collection of Data Science Interview Questions Solved in Python and Spark 
By: Antonio Gulli

1. What are the most important machine learning techniques?

Machine learning techniques can be divided into **supervised** and
**unsupervised**. Supervised techniques used labelled examples to generate a
model, meaning that for those examples we know the correct answer (a label or
result) in unsupervised techniques, the algorithm must find the answer.
Another way of separating the different techniques is through the task to
solve,which can be:
- regression
- classification
- clustering
- rule learning
- Density estimation

Algorithms for regression:

- Linear regession (univariate, multivariate)
- logistic regression (univariate, multinomial)
- Neural networks (perceptron)

Algorithms for Classification:

- Naive Bayes
- SVM
- Decision trees (and random forests)
- Neural networks

Algorithms for rule learning:

- A-priori
- Agent based rule learning

Algorithms for Clustering

- K-Means
- Hierarchical clustering
- Chained clustering
- Gaussian 

Algorithms for probability density estimation

- Expectation maximization

2. Why is it important to have a robust set of metrics for machine learning?

To evaluate the quality of results, compare different models and help decide if a model needs to be retrained, or decomissioned. Different metrics are used to evaluate different kinds of models:

## Metrics to evaluate classification models

- error matrix, (True Positives, True Negatives, False Positives, False Negatives)
- Accuracy (TP/Total predictions)
- Precision (TP/TP+FP)
- Recall (TP/TP+FN)
- F1-Score (2 * Precision * Recall/ (Precision+Recall))
- Sensitivity = Recall
- Specificity (TN/TN+FP)
- ROC curve: Receiver Operating Characteristic Curve, compares the true positive rate vs the false positive rate for various thresholds for classification (if model is probabilistic)
- AUC: Area under the ROC curve


## Metrics to evaluate regression models

- MSE: mean squared error
- RMSE: Root means squared error
- MAE: Mean Absolute error
- R-squared: coefficient of determination
- MAPE: Mean Absolute Percentage Error

## Metrics to evaluate Natural Language models

In this case we depend on the task for the model, such as translation, accuracy of question answering systems

- BLEU Score
- ROUGE Score
- Perplexity






