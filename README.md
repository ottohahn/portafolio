

# Portafolio

## Introduction

This portafolio showcases different abilities using jupyter notebooks for different data processing and machine learning tasks in linux, SQL and python. The contents are organized
by using small projects and either markdown files or Jupyter notebooks where needed and available. Some projects refer to a small bank I invented for this purpose, while others use synthetic datasets generated for the purpose of having data in the format needed for such a model. 

## Table of contents

- [ ] Exploring a folder's file structure (Linux command line exercises)
- [ ] Design a database using normalization
- [ ] Designing a data warehouse (dimensional modeling)
- [ ] Analyzing customer populations (SQL basic)
- [ ] Analyzing the products by customer (SQL medium)
- [ ] Creating a regression model in SQL (SQL advanced)
- [ ] Data quality and data cleansing
- [ ] Linear regression
- [ ] Logistic regression
- [ ] k-Nearest neighbours
- [ ] Decision trees
- [ ] Random forest
- [ ] Clustering
- [ ] Support vector machines
- [ ] Time series analysis
- [ ] Naive Bayes
- [ ] Principal components analysis
- [ ] Bayesian networks
- [ ] Similarity learning
- [ ] Particle Swarms
- [ ] Perceptron 
- [ ] Time series forecasting
- [ ] Language determination
- [ ] Sentiment analysis
- [ ] Named entity recognition
- [ ] Image recognition

## How to run the examples in this portafolio

To download and run an example, look for the data in the `data` folder and the source code in the `src` folder. and use the instructions given below:

|Project|data files|source code | instructions |
|-------|----------|------------|--------------|


## Authors and acknowledgment

Otto Hahn Herrera

## License

This project uses the [Do What the Fuck You Want to](https://choosealicense.com/licenses/wtfpl/) License

## Project status

This is a work in progress
